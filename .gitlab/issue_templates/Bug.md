<!---
Please read this!

Before opening a new issue, make sure to search for keywords in the issues
filtered by the "regression" or "bug" label:

- https://gitlab.com/mythicteam/mythictable/issues?label_name%5B%5D=Bug

and verify the issue you're about to submit isn't a duplicate.

Title suggestion: [Bug] Brief bug description
--->

# Summary
<!--Summarize the bug encountered concisely -->

## Steps to reproduce

## What is the current *bug* behavior?

## What is the expected *correct* behavior?

## Relevant logs and/or screenshots
<!--Paste any relevant logs - please use code blocks (```) to format console output,
logs, and code as it's tough to read otherwise. -->

## Possible fixes
<!-- If you can, link to the line of code that might be responsible for the problem -->

## Cause (Fill out after fix)
<!-- This must be filled out before the Bug can be closed.  
This is used in our post mordems to understand where we can make improvements. -->

Chose one or more:
- [ ] Work was rushed
- [ ] Plan Incomplete
- [ ] Insufficient Tests
- [ ] Missed in Review

/label ~"type::bug"
