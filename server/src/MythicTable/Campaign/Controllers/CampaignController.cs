using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MythicTable.Campaign.Data;
using MythicTable.Campaign.Util;
using MythicTable.Profile.Data;
using MythicTable.Collections.Providers;
using MythicTable.Common.Extensions.Controllers;

namespace MythicTable.Campaign.Controllers
{
    [Route("api/campaigns")]
    [ApiController]
    [Authorize]
    public class CampaignController : ControllerBase
    {
        private readonly ICampaignProvider campaignProvider;
        private readonly ICollectionProvider collectionProvider;
        private readonly IProfileProvider profileProvider;

        public CampaignController(ICampaignProvider campaignProvider, ICollectionProvider collectionProvider, IProfileProvider profileProvider)
        {
            this.campaignProvider = campaignProvider ?? throw new ArgumentNullException(nameof(campaignProvider));
            this.collectionProvider = collectionProvider;
            this.profileProvider = profileProvider;
        }

        // GET: api/Campaigns
        [HttpGet]
        public async Task<ActionResult<List<CampaignDTO>>> GetCampaigns()
        {
            var campaigns = await this.campaignProvider.GetAll(this.GetUserId());

            return campaigns.Select(campaign => campaign as CampaignDTO).ToList();
        }

        // GET: api/Campaigns/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CampaignDTO>> GetCampaign(string id)
        {
            var campaign = await this.campaignProvider.Get(id);

            return campaign as CampaignDTO;
        }

        // PUT: api/Campaigns/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut()]
        public async Task<IActionResult> PutCampaign(CampaignDTO campaign)
        {
            await campaignProvider.Update(campaign);

            return NoContent();
        }

        // POST: api/Campaigns
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<CampaignDTO>> PostCampaign(CampaignDTO campaign)
        {
            var createdCampaign = await CampaignUtil.CreateDefaultCampaign(this.GetUserId(), campaignProvider, collectionProvider, campaign);

            return CreatedAtAction(nameof(PostCampaign), new { id = campaign.Id }, createdCampaign as CampaignDTO);
        }

        // DELETE: api/Campaigns/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<CampaignDTO>> DeleteCampaign(string id)
        {
            var campaign = await campaignProvider.Get(id);
            await campaignProvider.Delete(id);
            return campaign as CampaignDTO;
        }  
        
        [HttpPut("{id}/join")]
        public async Task<ActionResult<CampaignDTO>> Join(string id)
        {
            var player = GetCurrentUser();

            return await campaignProvider.AddPlayer(id, player) as CampaignDTO;
        }
        
        [HttpPut("{id}/leave")]
        public async Task<ActionResult<CampaignDTO>> Leave(string id)
        {
            var player = GetCurrentUser();

            return await campaignProvider.RemovePlayer(id, player) as CampaignDTO;
        }

        // GET: api/campaigns/5/players
        [HttpGet("{id}/players")]
        public async Task<List<ProfileDto>> GetPlayers(string id)
        {
            List<PlayerDTO> players = await campaignProvider.GetPlayers(id);
            return await profileProvider.Get(players.Select(player => player.Name).ToArray());
        }

        // GET: api/campaigns/5/messages
        [HttpGet("{id}/messages")]
        public async Task<List<MessageDto>> GetMessages(string id)
        {
            return await campaignProvider.GetMessages(id);
        }

        private PlayerDTO GetCurrentUser()
        {
            return new PlayerDTO
            {
                Name = this.GetUserId()
            };
        }
    }
}
