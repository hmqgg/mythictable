using System;
using System.Threading.Tasks;
using MongoDB.Bson;
using MythicTable.Collections.Providers;
using MythicTable.Campaign.Data;

namespace MythicTable.Campaign.Util
{
    public class CampaignUtil
    {
        public static async Task<CampaignDTO> CreateDefaultCampaign(string owner, ICampaignProvider campaignProvider, ICollectionProvider collectionProvider, CampaignDTO campaign) 
        {
            var createdCampaign = await campaignProvider.Create(campaign, owner);

            var scene = await collectionProvider.CreateByCampaign(owner, "scenes", campaign.Id, SceneUtil.CreateScene("/static/assets/hideout.png", 29, 20));

            // TODO - Add characters

            return createdCampaign;
        }

        public static async Task<CampaignDTO> CreateTutorialCampaign(string owner, ICampaignProvider campaignProvider, ICollectionProvider collectionProvider)
        {
            var campaign = new CampaignDTO
            {
                Name = "Tutorial Campaign",
                TutorialCampaign = true,
                Description = "A campaign designed to get you familiar with the features of MythicTable",
                Created = DateTime.Now,
                ImageUrl = "/static/assets/tutorial/campaign-banner.jpg"
            };
            var createdCampaign = await campaignProvider.Create(campaign, owner);

            // Create characters

            await collectionProvider.CreateByCampaign(
                owner,
                "characters",
                campaign.Id,
                CharacterUtil.CreateCollectionCharacter(new BsonObjectId(ObjectId.GenerateNewId()), "marc", "Marc", "Human fighter", null, null)
            );
            await collectionProvider.CreateByCampaign(
                owner,
                "characters",
                campaign.Id,
                CharacterUtil.CreateCollectionCharacter(new BsonObjectId(ObjectId.GenerateNewId()), "sarah", "Sarah", "Elven wizard", null, null)
            );
            await collectionProvider.CreateByCampaign(
                owner,
                "characters",
                campaign.Id,
                CharacterUtil.CreateCollectionCharacter(new BsonObjectId(ObjectId.GenerateNewId()), "Redcap", "Redcap", "Goblin rogue", null, null)
            );

            // Create scenes

            var scene1 = SceneUtil.CreateScene("/static/assets/tutorial/basic-map-interactions.png", 50, 30, 86.5);
            scene1 = await collectionProvider.CreateByCampaign(owner, "scenes", campaign.Id, scene1);

            var scene2 = SceneUtil.CreateScene("/static/assets/tutorial/drawing-tools-chat.png", 50, 30, 86.5);
            await collectionProvider.CreateByCampaign(owner, "scenes", campaign.Id, scene2);

            var scene3 = SceneUtil.CreateScene("/static/assets/tutorial/thank-you.png", 50, 30, 86.5);
            await collectionProvider.CreateByCampaign(owner, "scenes", campaign.Id, scene3);

            // Add tokens to scenes

            await collectionProvider.CreateByCampaign(
                owner,
                "tokens",
                campaign.Id,
                CharacterUtil.CreateCollectionToken(new BsonObjectId(ObjectId.GenerateNewId()), "marc", "Marc", "Human fighter", null, null, "marcToken", scene1.GetId(), 4, 6, null)
            );
            await collectionProvider.CreateByCampaign(
                owner,
                "tokens",
                campaign.Id,
                CharacterUtil.CreateCollectionToken(new BsonObjectId(ObjectId.GenerateNewId()), "sarah", "Sarah", "Elven wizard", null, null, "sarahToken", scene1.GetId(), 28, 19, null)
            );
            await collectionProvider.CreateByCampaign(
                owner,
                "tokens",
                campaign.Id,
                CharacterUtil.CreateCollectionToken(new BsonObjectId(ObjectId.GenerateNewId()), "Wolf", "Wolf", "Big bad wolf", null, null, "wolfToken", scene1.GetId(), 25, 6, null, 3)
            );
            await collectionProvider.CreateByCampaign(
                owner,
                "tokens",
                campaign.Id,
                CharacterUtil.CreateCollectionToken(new BsonObjectId(ObjectId.GenerateNewId()), "Redcap", "Goblin 1", "Goblin rogue", null, null, "redcapToken1", scene1.GetId(), 27, 7, null, 1)
            );
            await collectionProvider.CreateByCampaign(
                owner,
                "tokens",
                campaign.Id,
                CharacterUtil.CreateCollectionToken(new BsonObjectId(ObjectId.GenerateNewId()), "Redcap", "Goblin 2", "Goblin rogue", null, null, "redcapToken2", scene1.GetId(), 26, 8, null, 1)
            );
            await collectionProvider.CreateByCampaign(
                owner,
                "tokens",
                campaign.Id,
                CharacterUtil.CreateCollectionToken(new BsonObjectId(ObjectId.GenerateNewId()), "Redcap", "Goblin 3", "Goblin rogue", null, null, "redcapToken3", scene1.GetId(), 24, 6, null, 1)
            );
            
            await collectionProvider.CreateByCampaign(
                owner,
                "tokens",
                campaign.Id,
                CharacterUtil.CreateCollectionToken(new BsonObjectId(ObjectId.GenerateNewId()), "sarah", "Sarah", "Elven wizard", null, null, "sarahToken", scene2.GetId(), 6, 6, null)
            );
            await collectionProvider.CreateByCampaign(
                owner,
                "tokens",
                campaign.Id,
                CharacterUtil.CreateCollectionToken(new BsonObjectId(ObjectId.GenerateNewId()), "Redcap", "Goblin 1", "Goblin rogue", null, null, "redcapToken1", scene2.GetId(), 9, 6, null, 1)
            );
            await collectionProvider.CreateByCampaign(
                owner,
                "tokens",
                campaign.Id,
                CharacterUtil.CreateCollectionToken(new BsonObjectId(ObjectId.GenerateNewId()), "Redcap", "Goblin 2", "Goblin rogue", null, null, "redcapToken2", scene2.GetId(), 9, 7, null, 1)
            );
            await collectionProvider.CreateByCampaign(
                owner,
                "tokens",
                campaign.Id,
                CharacterUtil.CreateCollectionToken(new BsonObjectId(ObjectId.GenerateNewId()), "Redcap", "Goblin 3", "Goblin rogue", null, null, "redcapToken3", scene2.GetId(), 8, 6, null, 1)
            );
            
            await collectionProvider.CreateByCampaign(
                owner,
                "tokens",
                campaign.Id,
                CharacterUtil.CreateCollectionToken(new BsonObjectId(ObjectId.GenerateNewId()), "marcOld", "Marc", "Elven fighter", null, null, "elfToken", scene2.GetId(), 26, 5, null)
            );
            await collectionProvider.CreateByCampaign(
                owner,
                "tokens",
                campaign.Id,
                CharacterUtil.CreateCollectionToken(new BsonObjectId(ObjectId.GenerateNewId()), "Tauren", "Ogre", "Ogre mauler", null, null, "ogreToken", scene2.GetId(), 28, 4, null, 3)
            );


            await collectionProvider.CreateByCampaign(owner, "players", campaign.Id, SceneUtil.CreatePlayer(scene1.GetId(), owner));

            return createdCampaign;
        }
    }
}
