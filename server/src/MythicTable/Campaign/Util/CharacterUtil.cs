using MongoDB.Bson;
using MythicTable.Campaign.Data;
using Newtonsoft.Json.Linq;

namespace MythicTable.Campaign.Util
{
    public class CharacterUtil
    {
        public static JObject CreateCollectionCharacter(BsonObjectId id, string image, string name, string description, string borderMode, string borderColor)
        {
            return JObject.Parse($@"{{
                _character_version: 1,
                _id: '{id}',
                image: '/static/assets/{image}.png',
                name: '{name}',
                description: '{description}',
                borderMode: '{borderMode}',
                borderColor: '{borderColor}',
                tokenSize: 1,
            }}");
        }

        public static JObject CreateCollectionToken(BsonObjectId id, string image, string name, string description, string borderMode, string borderColor, string tokenId, string sceneId, int x, int y, string backgroundColor, int tokenSize = 2)
        {
            return JObject.Parse($@"{{
                _character_version: 1,
                _token_version: 1,
                _id: '{tokenId}',
                image: '/static/assets/{image}.png',
                name: '{name}',
                description: '{description}',
                borderMode: '{borderMode}',
                borderColor: '{borderColor}',
                tokenSize: {tokenSize},
                sceneId: '{sceneId}',
                pos: {{
                    q: {x},
                    r: {y}
                }},
                backgroundColor: '{backgroundColor}',
                character: '{id}'
            }}");
        }
    }
}
