using Newtonsoft.Json.Linq;

namespace MythicTable.Campaign.Util
{
    public class SceneUtil
    {
        public static JObject CreateScene(string image, double q, double r, double size = 50)
        {
            return JObject.Parse($@"{{
                scene: {{ stage: '.' }},
                stage: {{
                    grid: {{ type: 'square', size: {size} }},
                    bounds: {{
                        nw: {{ q: 0, r: 0 }},
                        se: {{ q: {q}, r: {r} }},
                    }},
                    color: '#223344',
                    elements: [ {{
                        id: 'background',
                        asset: {{ kind: 'image', src: '{image}' }},
                        pos: {{ q: 0, r: 0, pa: '00' }},
                    }},
                    ],
                }},
            }}");
        }

        public static JObject CreatePlayer(string sceneId, string userId)
        {
            return JObject.Parse($@"{{
                sceneId: '{sceneId}',
                userId: '{userId}',
            }}");
        }
    }
}
