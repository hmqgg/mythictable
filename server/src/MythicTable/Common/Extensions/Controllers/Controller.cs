using Microsoft.AspNetCore.Mvc;

namespace MythicTable.Common.Extensions.Controllers
{
    public static class ControllerExtensions
    {
        public static string GetUserId(this ControllerBase controller)
        {
            return controller.HttpContext.User.FindFirst("preferred_username")?.Value;
        }
    }
}
