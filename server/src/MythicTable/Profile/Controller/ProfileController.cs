﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MythicTable.Common.Extensions.Controllers;
using MythicTable.Profile.Data;
using MythicTable.Profile.Exceptions;
using MythicTable.Profile.Util;

using MythicTable.Campaign.Data;
using MythicTable.Campaign.Util;
using MythicTable.Collections.Providers;

namespace MythicTable.Profile.Controller
{
    [Route("api/profiles")]
    [ApiController]
    public class ProfileController : ControllerBase
    {
        private readonly IProfileProvider provider;
        private readonly ICampaignProvider campaignProvider;
        private readonly ICollectionProvider collectionProvider;

        public ProfileController(IProfileProvider provider, ICampaignProvider campaignProvider, ICollectionProvider collectionProvider)
        {
            this.provider = provider;
            this.campaignProvider = campaignProvider;
            this.collectionProvider = collectionProvider;
        }

        [Authorize]
        [HttpGet("me")]
        public async Task<ProfileDto> Me()
        {
            var userId = this.GetUserId();
            try
            {
                return await provider.Get(userId);
            }
            catch (ProfileNotFoundException)
            {
                // TODO - Come up with a better way create a display name 
                // https://gitlab.com/mythicteam/mythictable/-/issues/145
                var displayName = userId.Split("@")[0];
                var dto = new ProfileDto
                {
                    DisplayName = displayName,
                    ImageUrl = ProfileUtil.GetRandomImage()
                };
                ProfileDto profile = await provider.Create(dto, userId);
                await this.FirstTimeSetup(userId);
                return profile;
            }
        }

        [HttpGet("{userId}")]
        public async Task<ProfileDto> Get(string userId)
        {
            return await provider.Get(userId);
        }

        [HttpGet]
        public async Task<List<ProfileDto>> Get([FromQuery(Name = "userId")]  List<string> userIds)
        {
            return await provider.Get(userIds.ToArray());
        }

        [Authorize]
        [HttpPut()]
        public async Task<ProfileDto> Put(ProfileDto dto)
        {
            var user = this.GetUserId();
            if (user != dto.UserId)
            {
                throw new ProfileNotAuthorizedException($"User: '{user}' is not authorized to update profile for user: '{dto.UserId}'");
            }
            return await provider.Update(dto);
        }

        private async Task<bool> FirstTimeSetup(string userId)
        {
            await CampaignUtil.CreateTutorialCampaign(userId, campaignProvider, collectionProvider);
            return true;
        }
    }
}
