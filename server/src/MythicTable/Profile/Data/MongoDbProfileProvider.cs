using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using MongoDB.Driver;
using MythicTable.Profile.Exceptions;

namespace MythicTable.Profile.Data
{
    public class MongoDbProfileProvider : IProfileProvider
    {
        private readonly IMongoCollection<ProfileDto> collection;
        private ILogger<MongoDbProfileProvider> logger;

        public MongoDbProfileProvider(MongoDbSettings settings, IMongoClient client, ILogger<MongoDbProfileProvider> logger)
        {
            var database = client.GetDatabase(settings.DatabaseName);
            collection = database.GetCollection<ProfileDto>("profiles");
            this.logger = logger;
        }

        public async Task<ProfileDto> Get(string userId)
        {
            var filter = Builders<ProfileDto>.Filter.Eq("UserId", userId);
            var dto = await collection.Find(filter).FirstOrDefaultAsync();
            if (dto != null) return dto;
            
            var message = $"Cannot find user: {userId}";
            logger.LogError(message);
            throw new ProfileNotFoundException(message);
        }

        public async Task<List<ProfileDto>> Get(string[] userIds)
        {
            var profileDtos = new List<ProfileDto>();
            foreach (var userId in userIds)
            {
                try
                {
                    profileDtos.Add(await Get(userId));
                }
                catch (ProfileNotFoundException)
                {
                }
            }

            return profileDtos;
        }

        public async Task<ProfileDto> Create(ProfileDto profile, string userId)
        {
            if (profile == null)
            {
                throw new ProfileInvalidException($"The profile is null");
            }

            if (!string.IsNullOrEmpty(profile.Id))
            {
                throw new ProfileInvalidException($"The profile already has an id");
            }
            profile.UserId = userId;
            await collection.InsertOneAsync(profile);
            return profile;
        }

        public async Task<ProfileDto> Update(ProfileDto profile)
        {
            if (profile == null)
            {
                var message = "The profile is null";
                logger.LogError(message);
                throw new ProfileInvalidException(message);
            }

            if (string.IsNullOrEmpty(profile.UserId))
            {
                var message = "The profile MUST have an id";
                logger.LogError(message);
                throw new ProfileInvalidException(message);
            }

            await collection.ReplaceOneAsync(p => p.Id == profile.Id, profile);
            return profile;
        }

        public async Task Delete(string userId)
        {
            try
            {
                await Get(userId);
            }
            catch (ProfileNotFoundException)
            {
                throw new ProfileNotFoundException($"Profile for user: {userId} doesn't exist");
            }
            var results = await collection.DeleteOneAsync(p => p.UserId == userId);
            if (results.DeletedCount == 0)
            {
                throw new ProfileNotFoundException($"Unable to delete profile: {userId}");
            }
        }
    }
}
