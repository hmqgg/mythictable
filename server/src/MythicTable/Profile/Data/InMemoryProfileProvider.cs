using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using MythicTable.Profile.Exceptions;

namespace MythicTable.Profile.Data
{
    public class InMemoryProfileProvider : IProfileProvider
    {
        private Dictionary<string, ProfileDto> profiles;
        private ILogger<InMemoryProfileProvider> logger;
        public InMemoryProfileProvider(ILogger<InMemoryProfileProvider> logger)
        {
            profiles = new Dictionary<string, ProfileDto>();
            this.logger = logger;
        }

        public Task<ProfileDto> Get(string userId)
        {
            ProfileDto profile;
            profiles.TryGetValue(userId, out profile);
            if (profile == null)
            {
                var message = $"Cannot find user: {userId}";
                logger.LogError(message);
                throw new ProfileNotFoundException(message);
            }

            return Task.FromResult(profile);
        }

        public async Task<List<ProfileDto>> Get(string[] userIds)
        {
            var dtos = new List<ProfileDto>();
            foreach (var userId in userIds)
            {
                try
                {
                    dtos.Add(await Get(userId));
                }
                catch (ProfileNotFoundException)
                {
                }
            }

            return dtos;
        }

        public Task<ProfileDto> Create(ProfileDto profile, string userId)
        {
            if (profile == null)
            {
                throw new ProfileInvalidException($"The profile is null");
            }

            if (!string.IsNullOrEmpty(profile.Id))
            {
                throw new ProfileInvalidException($"The profile already has an id");
            }

            profile.UserId = userId;
            profile.Id = Guid.NewGuid().ToString();
            this.profiles[profile.UserId] = profile;
            return Task.FromResult(profile);
        }

        public Task<ProfileDto> Update(ProfileDto profile)
        {
            if (profile == null)
            {
                var message = "The profile is null";
                logger.LogError(message);
                throw new ProfileInvalidException(message);
            }

            if (string.IsNullOrEmpty(profile.UserId))
            {
                var message = "The profile MUST have an id";
                logger.LogError(message);
                throw new ProfileInvalidException(message);
            }

            profiles[profile.UserId] = profile;
            return Task.FromResult(profile);
        }

        public Task Delete(string userId)
        {
            try
            {
                this.Get(userId);
                profiles.Remove(userId);
                return Task.CompletedTask;
            }
            catch (ProfileNotFoundException)
            {
                throw new ProfileNotFoundException($"Profile for user: {userId} doesn't exist");
            }
        }
    }
}
