﻿using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Mongo.Migration.Startup.Static;
using Mongo2Go;
using MongoDB.Driver;
using Moq;
using MythicTable.Profile.Data;
using MythicTable.Tests.Profile.Data;
using Xunit;

namespace MythicTable.Integration.Tests.Profile.Data
{
    public class MongoDbProfileProviderTest : AbstractProfileProviderTest<MongoDbProfileProvider>, IAsyncLifetime
    {
        private MongoDbProfileProvider provider;
        protected override MongoDbProfileProvider Provider => provider;

        private Mock<ILogger<MongoDbProfileProvider>> loggerMock;
        protected override Mock<ILogger<MongoDbProfileProvider>> LoggerMock => loggerMock;

        private MongoDbRunner runner;

        public Task InitializeAsync()
        {
            runner = MongoDbRunner.Start(additionalMongodArguments: "--quiet");
            var settings = new MongoDbSettings
            {
                ConnectionString = runner.ConnectionString,
                DatabaseName = "mythictable"
            };
            var client = new MongoClient(settings.ConnectionString);
            loggerMock = new Mock<ILogger<MongoDbProfileProvider>>();
            provider = new MongoDbProfileProvider(settings, client, LoggerMock.Object);
            return Task.CompletedTask;
        }

        public Task DisposeAsync()
        {
            MongoMigrationClient.Reset();
            runner.Dispose();
            return Task.CompletedTask;
        }
    }
}
