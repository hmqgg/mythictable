﻿using Microsoft.Extensions.Logging;
using Moq;
using MythicTable.Profile.Data;

namespace MythicTable.Tests.Profile.Data
{
    public class InMemoryProfileProviderTest: AbstractProfileProviderTest<InMemoryProfileProvider>
    {
        protected override InMemoryProfileProvider Provider { get; }

        protected override Mock<ILogger<InMemoryProfileProvider>> LoggerMock { get; }

        public InMemoryProfileProviderTest()
        {
            LoggerMock = new Mock<ILogger<InMemoryProfileProvider>>();
            Provider = new InMemoryProfileProvider(LoggerMock.Object);
        }
    }
}
