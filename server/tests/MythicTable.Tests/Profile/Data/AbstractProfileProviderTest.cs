﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Moq;
using MythicTable.Profile.Data;
using MythicTable.Profile.Exceptions;
using Xunit;

namespace MythicTable.Tests.Profile.Data
{
    public abstract class AbstractProfileProviderTest<T> where T : IProfileProvider
    {
        private const string UserId = "test-user";
        private const string SecondUserId = "other-user";

        protected abstract T Provider { get;  }
        protected abstract Mock<ILogger<T>> LoggerMock { get; }

        [Fact]
        public async Task CreatesAssignsId()
        {
            await Provider.Create(new ProfileDto(), UserId);
            var profile = await Provider.Get(UserId);
            Assert.Equal(UserId, profile.UserId);
        }

        [Fact]
        public async Task CreatesGeneratesId()
        {
            await Provider.Create(new ProfileDto(), UserId);
            var profile = await Provider.Get(UserId);
            Assert.NotNull(profile.Id);
        }

        [Fact]
        public async Task CreatesGeneratesUniqueId()
        {
            var profile1 = await Provider.Create(new ProfileDto(), UserId);
            var profile2 = await Provider.Create(new ProfileDto(), SecondUserId);
            Assert.NotEqual(profile1.Id, profile2.Id);
        }

        [Fact]
        public async Task CanDelete()
        {
            await Provider.Create(new ProfileDto(), UserId);
            var profile = await Provider.Get(UserId);
            await Provider.Delete(profile.UserId);
            await Assert.ThrowsAsync<ProfileNotFoundException>(() => Provider.Get(UserId));
        }

        [Fact]
        public async Task GetThrowsWhenProfileNotFound()
        {
            var exception = await Assert.ThrowsAsync<ProfileNotFoundException>(() => Provider.Get(UserId));
            Assert.Equal($"Cannot find user: {UserId}", exception.Message);
            VerifyLog(LogLevel.Error, $"Cannot find user: {UserId}");
        }

        [Fact]
        public async Task FailedDeleteLogsTheFailure()
        {
            var exception = await Assert.ThrowsAsync<ProfileNotFoundException>(() => Provider.Delete(UserId));
            Assert.Equal($"Profile for user: {UserId} doesn't exist", exception.Message);
            VerifyLog(LogLevel.Error, $"Cannot find user: {UserId}");
        }

        [Fact]
        public async Task FailedUpdateLogsTheFailure()
        {
            var exception = await Assert.ThrowsAsync<ProfileInvalidException>(() => Provider.Update(null));
            Assert.Equal("The profile is null", exception.Message);
            VerifyLog(LogLevel.Error, "The profile is null");
        }

        [Fact]
        public async Task UpdateRequiresProfileWithUserId()
        {
            var exception = await Assert.ThrowsAsync<ProfileInvalidException>(() => Provider.Update(new ProfileDto()));
            Assert.Equal("The profile MUST have an id", exception.Message);
            VerifyLog(LogLevel.Error, "The profile MUST have an id");
        }

        private void VerifyLog(LogLevel expectedLevel, string expected)
        {
            LoggerMock.Verify(l => l.Log(
                It.Is<LogLevel>(level => level == expectedLevel),
                It.IsAny<EventId>(),
                It.Is<It.IsAnyType>((v, t) => v.ToString() == expected),
                It.IsAny<Exception>(),
                It.Is<Func<It.IsAnyType, Exception, string>>((v, t) => true)));
        }
    }
}
