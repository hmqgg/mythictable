﻿import Vue from 'vue';
import router from './router.js';
import store from './store.js';
import VueGtag from 'vue-gtag';
import axios from 'axios';

import GameStateStore from './store/GameStateStore';
import actions from './core/ruleset/experiment/actions';

import { BootstrapVue, IconsPlugin, BootstrapVueIcons } from 'bootstrap-vue';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';

import VueChatScroll from 'vue-chat-scroll';
import VueMaterial from 'vue-material';
import 'vue-material/dist/vue-material.min.css';
import 'vue-material/dist/theme/default.css';

Vue.config.productionTip = false;

// Install BootstrapVue
Vue.use(BootstrapVue);
Vue.use(BootstrapVueIcons);
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin);

Vue.use(VueChatScroll);
Vue.use(VueMaterial);

import './assets/main.scss';
import './assets/modal.scss';

// FIXME: find a better place to load in the ruleset than main.js
const ruleset = {
    actions: actions,
};
GameStateStore.state.ruleset = ruleset;
Vue.use(VueGtag, {
    config: { id: process.env.VUE_APP_ANALYTICS },
    appName: 'Mythic Table',
    pageTrackerScreenviewEnabled: true,
});

// eslint-disable-next-line no-unused-vars
new Vue({
    el: '#app',
    store,
    router,
    render: h => h('router-view'),
});

// export { store };

// Set up axios interceptor to add API token
// TODO: Restrict this to only add the token if requesting from the original domain
// (so we don't send our SECRET auth token to other websites accidentally)
axios.interceptors.request.use(
    config => {
        if (!(config.hasOwnProperty('disableAuth') && config.disableAuth)) {
            config.headers['Authorization'] = `Bearer ${store.state.oidcStore.access_token}`;
        }
        return config;
    },
    error => {
        return Promise.reject(error);
    },
);
