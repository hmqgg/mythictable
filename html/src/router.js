import Vue from 'vue';
import VueRouter from 'vue-router';
import store from './store.js';
import { vuexOidcCreateRouterMiddleware } from 'vuex-oidc';
import { oidcSettings } from './oidc.js';

Vue.use(VueRouter);

const routes = [
    {
        path: '/play/:sessionId/:sceneId',
        name: 'live-play',
        component: () => import('./views/LivePlay'),
        props: true,
    },
    {
        path: '/$debug/*',
        component: () => import('./views/debug/DebugPage.vue'),
    },
    {
        path: '/$debug',
        redirect: '/$debug/',
    },
    {
        path: '/home',
        component: () => import('./views/Home.vue'),
        meta: { isPublic: true },
    },
    {
        path: '/oidc',
        name: 'oidcCallback',
        component: () => import('./views/OidcCallback.vue'),
    },
    {
        path: '/',
        name: 'layout',
        component: () => import('./views/layout/TheLayout'),
        children: [
            {
                path: 'invite/:id',
                component: () => import('./views/campaign-management/CampaignInvitation'),
                name: 'campaign-invite',
            },
            {
                path: 'edit/:id?',
                component: () => import('./views/campaign-management/CampaignEdit'),
                name: 'campaign-edit',
                alias: 'create',
            },
            {
                path: '*',
                component: () => import('./views/campaign-management/CampaignList'),
                name: 'campaign-list',
            },
        ],
    },
    {
        path: '/ext/auth/profile',
        name: 'profile',
        redirect: () => {
            window.location.href = oidcSettings.authority + '/account';
            return '/redirecting';
        },
    },
];

const router = new VueRouter({
    routes,
    mode: 'history',
});
router.beforeEach(vuexOidcCreateRouterMiddleware(store));

export default router;
