export const COLLECTION_TYPES = {
    scenes: 'scenes',
    players: 'players',
    characters: 'characters',
    tokens: 'tokens',
};
