const ErrorStore = {
    namespaced: true,
    state: {
        modalMessage: '',
    },
    actions: {
        clear({ state }) {
            state.modalMessage = '';
        },
        modal({ state }, message) {
            state.modalMessage = message;
        },
    },
};

export default ErrorStore;
