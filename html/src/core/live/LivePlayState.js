const LivePlayState = {
    namespaced: true,
    state: {
        connected: false,
        sessionId: null,
        director: null,
    },
    mutations: {
        setDirector(state, director) {
            state.director = director;
        },
        setSessionId(state, sessionId) {
            state.sessionId = sessionId;
        },
    },
    actions: {
        addCharacter({ state }, { image, pos, sceneId }) {
            state.director.addCharacter(image, pos, sceneId);
        },
        removeCharacter({ state }, { characterId }) {
            state.director.removeCharacter(characterId);
        },
    },
};

export default LivePlayState;
