// Use require() instead of import as SignalR does not use ES6-style export
const signalR = require('@microsoft/signalr');
import axios from 'axios';
import Asset from '@/core/entity/Asset';
import getAllImages from '@/components/scene/SceneHelper.js';

class LivePlayDirector {
    constructor(store) {
        this.store = store;
        this.connection = null;
        store.commit('live/setDirector', this);
    }

    get sessionId() {
        return this.state.sessionId;
    }
    set sessionId(id) {
        this.state.sessionId = id;
    }

    get state() {
        return this.store.state.live;
    }

    async init() {
        console.log(this.store.state);

        let accessToken = this.store.state.oidcStore.access_token;
        this.connection = new signalR.HubConnectionBuilder()
            .withUrl('/api/live', { accessTokenFactory: () => accessToken })
            .withAutomaticReconnect()
            .build();

        this.connection.on('ConfirmOpDelta', this.onConfirmDelta.bind(this));
        this.connection.on('CharacterAdded', this.onCharacterAdded.bind(this));
        this.connection.on('CharacterRemoved', this.onCharacterRemoved.bind(this));
        this.connection.on('ExceptionRaised', this.onExceptionRaised.bind(this));
        this.connection.on('SendMessage', this.onMessageReceived.bind(this));
        this.connection.on('ObjectUpdated', this.onObjectUpdated.bind(this));
        this.connection.on('ObjectAdded', this.onObjectAdded.bind(this));
        this.connection.on('ObjectRemoved', this.onObjectRemoved.bind(this));
        this.connection.on('DrawLine', this.onLineDrawn.bind(this));
        this.connection.onclose(() => {
            // FIXME: PoC only; needs to be mutation if used in prod
            this.store.state.live.connected = false;
        });
    }

    async connect() {
        const promise = this.connection.start();
        try {
            await promise;
        } catch (e) {
            this.state.connected = false;
            throw e;
        }
        this.state.connected = true; // FIXME: PoC only; needs to be mutation if used in prod
        let success = await this.connection.invoke('joinSession', this.sessionId);
        if (!success) {
            // Failed to join the session: might be unauthorized, or it might not exist
            this.store.dispatch('errors/modal', 'Unable to join session.');
        }
        await this.initializeEntities();
    }

    async disconnect() {
        await this.connection.stop();
        this.state.connected = false;
    }

    async initializeEntities() {
        // TODO - Load characters???
        axios
            .get(`/api/campaigns/${this.sessionId}/messages`)
            .then(response => {
                let patch = [];
                response.data.forEach(roll => {
                    patch.push({ op: 'add', path: '/global/rollLog/-', value: roll });
                });
                this.store.dispatch('gamestate/applyDelta', patch);
            })
            .catch(error => {
                console.error(error);
            });
        await this.store.dispatch('players/load');
        const scenes = await this.store.dispatch('collections/loadScenes');
        await this.postLoadScenes(scenes);
        this.store.dispatch('gamestate/setBase');
    }

    onConfirmDelta(sessionDelta) {
        this.store.dispatch('gamestate/applyDelta', sessionDelta.delta);
    }

    onCharacterAdded(characterDto) {
        this.store.dispatch('gamestate/entities/load', [characterDto]);
        Asset.loadAll([characterDto]);
    }

    onCharacterRemoved(characterId) {
        const patch = { op: 'remove', path: `/entities/${characterId}` };
        console.log('onCharRemoved');
        this.store.dispatch('gamestate/patch', patch);
        this.store.dispatch('gamestate/entities/remove', characterId);
    }

    onExceptionRaised(error) {
        console.err('Exception raised by server: ' + error);
    }

    async addCharacter(image, pos, sceneId) {
        const request = { campaignId: this.sessionId, x: pos.q, y: pos.r, image, sceneId };
        await this.connection.invoke('AddCharacter', this.sessionId, request);
    }

    async removeCharacter(characterId) {
        const request = { campaignId: this.sessionId, characterId: characterId };
        await this.connection.invoke('RemoveCharacter', this.sessionId, request);
    }

    submitRoll(diceObject) {
        this.tryRollDice(diceObject);
    }
    async tryRollDice(diceObject) {
        //TODO Check for valid diceObject
        await this.connection.invoke('SendMessage', this.sessionId, diceObject);
    }
    onMessageReceived(message) {
        let patch = { op: 'add', path: '/global/rollLog/-', value: message };
        this.store.dispatch('gamestate/applyDelta', patch);
    }

    async updateCampaignObject(collection, id, patch) {
        const payload = { collection, campaignId: this.sessionId, id, patch };
        return await this.connection.invoke('UpdateObject', this.sessionId, payload);
    }

    async addCampaignObject(collection, scene) {
        return await this.connection.invoke('AddCollectionItem', this.sessionId, collection, this.sessionId, scene);
    }

    async removeCampaignObject(collection, id) {
        const success = await this.connection.invoke('RemoveObject', this.sessionId, collection, id);
        if (!success) {
            console.warn(`Collection object ${id} was not deleted`);
        }
        return success;
    }

    async drawLine(sceneId, line) {
        await this.connection.invoke('DrawLine', this.sessionId, line);
    }

    onLineDrawn(line) {
        this.store.dispatch('drawing/lineDrawReceived', { line });
    }

    onObjectUpdated(parameters) {
        this.store.dispatch('collections/onUpdated', parameters);
    }

    onObjectAdded(collection, scene) {
        this.store.dispatch('collections/onAdded', { collection, item: scene });
    }

    onObjectRemoved(collection, id) {
        this.store.dispatch('collections/onRemoved', { collection, id });
    }

    async postLoadScenes(scenes) {
        const images = getAllImages(scenes);
        await Asset.loadAll(images);
        this.store.dispatch('gamestate/activateScene', scenes[0]);
    }
}

export { LivePlayDirector as default };
