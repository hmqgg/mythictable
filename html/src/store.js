import Vue from 'vue';
import Vuex from 'vuex';
import { vuexOidcCreateStoreModule } from 'vuex-oidc';

import GameStateStore from './store/GameStateStore';
import { oidcSettings } from './oidc';
import LivePlayState from './core/live/LivePlayState';
import AssetStore from './core/assets/AssetStore';
import DropperStore from './core/dropper/store/DropperStore';
import LibraryStore from './core/library/store/LibraryStore';
import CollectionStore from './core/collections/store';
import UserCollectionStore from './core/collections/user/UserCollectionStore';
import PlayerStore from './core/collections/players/store';
import DrawingStore from './core/drawing/store';
import ProfileStore from './core/profile/store';
import CharacterStore from './core/collections/characters/store';
import TokenStore from './core/collections/tokens/store';
import ErrorStore from './core/errors/store';

Vue.use(Vuex);

const store = new Vuex.Store({
    modules: {
        live: LivePlayState,
        gamestate: GameStateStore,
        oidcStore: vuexOidcCreateStoreModule(
            oidcSettings,
            {
                dispatchEventsOnWindow: true,
            },
            {
                // remove in prod
                userLoaded: user => console.log('OIDC user is loaded:', user),
                userUnloaded: () => console.log('OIDC user is unloaded'),
                accessTokenExpiring: () => console.log('Access token will expire'),
                accessTokenExpired: () => console.log('Access token did expire'),
                silentRenewError: () => console.log('OIDC user is unloaded'),
                userSignedOut: () => console.log('OIDC user is signed out'),
                oidcError: payload => console.log(`An error occured at ${payload.context}:`, payload.error),
            },
        ),
        assets: AssetStore,
        dropper: DropperStore,
        library: LibraryStore,
        collections: CollectionStore,
        userCollections: UserCollectionStore,
        players: PlayerStore,
        characters: CharacterStore,
        tokens: TokenStore,
        drawing: DrawingStore,
        profile: ProfileStore,
        errors: ErrorStore,
    },
});

export default store;
