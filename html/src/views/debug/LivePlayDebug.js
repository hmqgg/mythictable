import Asset from '@/core/entity/Asset';

async function loadDebugScene({ vm, sceneId }) {
    if (sceneId == 'debug') {
        const data = await import('./data_demo');
        await data.loader(vm.$store);

        Asset.loadAll(vm.$store.state.gamestate.entities);
    }
}

export { loadDebugScene };
