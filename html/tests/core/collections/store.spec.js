import _ from 'lodash';
import CollectionStore from '@/core/collections/store';

const defaultState = {
    scenes: {
        id01: { _id: 'id01', foo: 'bar' },
        id02: { _id: 'id02', foo: 'barbar' },
    },
    characters: {
        id03: { _id: 'id03', foo: 'char' },
        id04: { _id: 'id04', foo: 'charbar' },
    },
};

describe('CollectionStore', () => {
    const getItem = CollectionStore.getters.getItem;
    let state;

    beforeEach(() => {
        state = _.cloneDeep(defaultState);
    });

    describe('getters', () => {
        describe('getItem()', () => {
            it('returns scene by ID', () => {
                const result = getItem(state)('scenes', 'id01');
                expect(result).toBe(state.scenes.id01);
            });

            it('returns null when no collection', () => {
                const result = getItem(state)('shoes', 'id01');
                expect(result).toBeNull();
            });

            it('returns null when no item is found', () => {
                const result = getItem(state)('characters', 'id01');
                expect(result).toBeNull();
            });
        });

        describe('getCollection()', () => {
            const getCollection = CollectionStore.getters.getCollection;
            it('returns collection', () => {
                const result = getCollection(state)('scenes');
                expect(result).toBe(state.scenes);
            });

            it('empty list when no collection', () => {
                const result = getCollection(state)('shoes');
                expect(result).toEqual([]);
            });
        });
    });

    describe('mutations', () => {
        describe('patch', () => {
            const doPatch = CollectionStore.mutations.patch;

            it('performs basic patch', () => {
                const patch = [
                    {
                        op: 'replace',
                        path: '/foo',
                        value: 'bar2',
                    },
                ];
                doPatch(state, { collection: 'scenes', id: 'id01', patch });
                const result = getItem(state)('scenes', 'id01');
                expect(result.foo).toEqual('bar2');
            });
        });

        describe('add', () => {
            const add = CollectionStore.mutations.add;

            it('adds new collection', () => {
                add(state, { collection: 'shoes', item: { _id: 'id05', foo: 'shoe' } });
                const result = getItem(state)('shoes', 'id05');
                expect(result.foo).toEqual('shoe');
            });
        });
    });

    describe('actions', () => {
        describe('onLoad', () => {
            const onLoad = CollectionStore.actions.onLoad;
            const add = CollectionStore.mutations.add;
            const mockCommit = function(p1, { collection, item }) {
                add(state, { collection, item });
            };

            it('performs basic onLoad', () => {
                const scenes = [
                    { _id: 'id05', foo: 'bar5' },
                    { _id: 'id06', foo: 'bar6' },
                ];
                onLoad({ commit: mockCommit }, { collection: 'scenes', items: scenes });
                let result = getItem(state)('scenes', 'id05');
                expect(result.foo).toEqual('bar5');
                result = getItem(state)('scenes', 'id06');
                expect(result.foo).toEqual('bar6');
            });

            it('handles duplicates and over-writes', () => {
                const scenes = [
                    { _id: 'id04', foo: 'over-written' },
                    { _id: 'id05', foo: 'bar5' },
                ];
                onLoad({ commit: mockCommit }, { collection: 'scenes', items: scenes });
                let result = getItem(state)('scenes', 'id04');
                expect(result.foo).toEqual('over-written');
                result = getItem(state)('scenes', 'id05');
                expect(result.foo).toEqual('bar5');
            });
        });
    });
});
